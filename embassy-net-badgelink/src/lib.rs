#![no_std]

use ch::driver::LinkState;
use embassy_futures::join::join;
use embassy_futures::select::{Either, select};
use embassy_net_driver_channel as ch;
use embedded_hal_async::serial::Write;
use esp32s3_hal::Uart;
use esp32s3_hal::uart::Instance;
use serial_line_ip::{Decoder, Encoder};
use heapless::Vec;

const MTU: usize = 1500;

pub type Device<'d> = embassy_net_driver_channel::Device<'d, MTU>;

pub struct State<const N_RX: usize, const N_TX: usize> {
    ch_state: ch::State<MTU, N_RX, N_TX>,
}

impl<const N_RX: usize, const N_TX: usize> State<N_RX, N_TX> {
    /// Create a new `State`.
    pub const fn new() -> Self {
        Self {
            ch_state: ch::State::new(),
        }
    }
}

pub struct Runner<'d, UART> {
    uart: UART,
    ch: ch::Runner<'d, MTU>,
    decoder: Decoder,
} 

impl<'d, UART> Runner<'d, Uart<'static, UART>>
where
    UART: Instance,
{
    pub async fn run(mut self) -> ! {
        let (state_chan, mut rx_chan, mut tx_chan) = self.ch.split();
        let mut read_buf = [0u8;64];
        state_chan.set_link_state(LinkState::Up);
        loop {
            match select(
                join(
                    self.uart.read(&mut read_buf),
                    rx_chan.rx_buf()
                ),
                tx_chan.tx_buf(),
            ).await {
                Either::First((_,p)) => {
                    if let Ok((num_bytes, _, end_of_packet)) = self.decoder.decode(&read_buf, p) {
                        if end_of_packet {
                            rx_chan.rx_done(num_bytes);
                        }
                    }
                }
                Either::Second(p) => {
                    let mut enc: Vec<u8, 1500> = Vec::new();
                    let mut encoder = Encoder::new();
                    encoder.encode(p, &mut enc).ok();
                    let num_bytes = encoder.finish(&mut enc).unwrap();
                    self.uart.write(&enc[0..num_bytes.written]).await.ok();
                    tx_chan.tx_done();
                }
            }
        }
    }
}

/// Obtain a driver for using the W5500 with [`embassy-net`](https://crates.io/crates/embassy-net).
pub fn new<'a, const N_RX: usize, const N_TX: usize, UART>(
    mac_addr: [u8; 6],
    state: &'a mut State<N_RX, N_TX>,
    uart: UART,
) -> (Device<'a>, Runner<'a, UART>) {
    let (runner, device) = ch::new(&mut state.ch_state, mac_addr);
    (
        device,
        Runner {
            ch: runner,
            uart,
            decoder: Decoder::new(),
        },
    )
}