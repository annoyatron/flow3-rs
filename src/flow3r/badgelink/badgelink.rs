
pub fn init_badgelink_io(i2c: I2cProxy<'static, XtensaMutex<I2C<'static, I2C0>>>) {
    let mut pe1 = port_expander::Max7321::new(shared_i2c.acquire_i2c(), true, true, true, false);

    let mut pe1_io = pe1.split();
    pe1_io.p1.set_high().unwrap();
    let _in_r_sw = pe1_io.p3;
    let _in_t_sw = pe1_io.p4;
    let _out_r_sw = pe1_io.p5;
    let _out_t_sw = pe1_io.p6;
}