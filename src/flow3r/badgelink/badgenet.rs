use embassy_executor::Spawner;
use embassy_net::{Config, Ipv6Address, Ipv6Cidr, Stack, StackResources, StaticConfigV6};
use embassy_net_badgelink::{Device, Runner, State};
use hal::{
    efuse::Efuse,
    peripherals::{UART0, UART1},
    Rng, Uart,
};
use heapless::Vec;
use static_cell::StaticCell;

static STATE: StaticCell<State<8, 8>> = StaticCell::new();
static STACK: StaticCell<Stack<Device<'static>>> = StaticCell::new();
static STACK_RESOURCES: StaticCell<StackResources<2>> = StaticCell::new();

pub async fn start_badgenet_left(uart: Uart<'static, UART0>, rng: &'static mut Rng<'static>) {
    let mac_addr = Efuse::get_mac_address();
    let state = STATE.init(State::<8, 8>::new());
    let (device, runner) = embassy_net_badgelink::new(mac_addr, state, uart);
    let stack = STACK.init(Stack::new(
        device,
        Config::ipv6_static(StaticConfigV6 {
            address: Ipv6Cidr::new(Ipv6Address::new(0, 0, 0, 0, 0, 0, 0, 0), 128),
            gateway: None,
            dns_servers: Vec::new(),
        }),
        STACK_RESOURCES.init(StackResources::new()),
        rng.random().into(),
    ));

    let spawner = Spawner::for_current_executor().await;
    spawner.spawn(runner_0_task(runner)).ok();
    spawner.spawn(stack_task(stack)).ok();
}

pub async fn start_badgenet_right(uart: Uart<'static, UART1>, rng: &'static mut Rng<'static>) {
    let mac_addr = Efuse::get_mac_address();
    let state = STATE.init(State::<8, 8>::new());
    let (device, runner) = embassy_net_badgelink::new(mac_addr, state, uart);
    let stack = STACK.init(Stack::new(
        device,
        Config::ipv6_static(StaticConfigV6 {
            address: Ipv6Cidr::new(Ipv6Address::new(0, 0, 0, 0, 0, 0, 0, 0), 128),
            gateway: None,
            dns_servers: Vec::new(),
        }),
        STACK_RESOURCES.init(StackResources::new()),
        rng.random().into(),
    ));

    let spawner = Spawner::for_current_executor().await;
    spawner.spawn(runner_1_task(runner)).ok();
    spawner.spawn(stack_task(stack)).ok();
}

#[embassy_executor::task]
pub async fn runner_0_task(runner: Runner<'static, Uart<'static, UART0>>) -> ! {
    runner.run().await
}

#[embassy_executor::task]
pub async fn runner_1_task(runner: Runner<'static, Uart<'static, UART1>>) -> ! {
    runner.run().await
}

#[embassy_executor::task(pool_size = 2)]
pub async fn stack_task(stack: &'static mut Stack<Device<'static>>) -> ! {
    stack.run().await
}
