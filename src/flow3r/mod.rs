use self::badgelink::BadgeLink;
use self::captouch::CaptouchHandler;
use self::display::Display;
use self::imu::ImuHandler;
use self::input::InputHandler;
use self::leds::Leds;

pub mod badgelink;
pub mod captouch;
pub mod display;
pub mod imu;
pub mod input;
pub mod leds;
pub mod sdcard;
pub mod ui;

pub struct Flow3r {
    pub badgelink: BadgeLink,
    pub captouch: CaptouchHandler,
    pub display: Display,
    pub imu: ImuHandler,
    pub inputs: InputHandler,
    pub leds: Leds,
}

impl Flow3r {
    pub fn new(
        badgelink: BadgeLink,
        captouch: CaptouchHandler,
        display: Display,
        imu: ImuHandler,
        inputs: InputHandler,
        leds: Leds,
    ) -> Self {
        Self {
            badgelink,
            captouch,
            display,
            imu,
            inputs,
            leds,
        }
    }
}
