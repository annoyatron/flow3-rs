use embassy_executor::{Executor, Spawner};
use embassy_time::{Duration, Timer};
use hal::{
    clock::{ClockControl, Clocks},
    cpu_control::CpuControl,
    embassy,
    gdma::Gdma,
    i2c::I2C,
    ledc::LEDC,
    peripherals::{Peripherals, I2C0},
    prelude::*,
    pulse_control::ClockSource,
    systimer::SystemTimer,
    timer::TimerGroup,
    PulseControl, Rng, Rtc, Spi, Uart, IO,
};
use static_cell::StaticCell;

use crate::flow3r::{
    badgelink::BadgeLink,
    captouch::{captouch_controller, CaptouchHandler},
    display::Display,
    imu::ImuHandler,
    input::{input_controller, InputHandler},
    leds::init_leds,
    Flow3r,
};
use crate::main;

const READ_BUF_SIZE: usize = 64;

static RNG: StaticCell<Rng> = StaticCell::new();
static EXECUTOR: StaticCell<Executor> = StaticCell::new();
//static APP_CORE_EXECUTOR: StaticCell<Executor> = StaticCell::new();
static CLOCKS: StaticCell<Clocks> = StaticCell::new();

pub fn start_runtime() -> ! {
    let executor = EXECUTOR.init(Executor::new());
    executor.run(|spawner| {
        spawner.spawn(init_runtime()).ok();
    });
}

#[embassy_executor::task]
async fn start_app_core(mut cpu_control: CpuControl) -> ! {
    let mut app_core_function = || {
        /*let executor = APP_CORE_EXECUTOR.init(Executor::new());
        executor.run(|spawner| {
            spawner.spawn(display_refresh()).ok();
        })*/
    };
    let _guard = cpu_control.start_app_core(&mut app_core_function).unwrap();
    loop {
        Timer::after(Duration::from_secs(100)).await;
    }
}

#[embassy_executor::task]
async fn init_runtime() {
    esp_println::println!("Init!");
    let peripherals = Peripherals::take();
    let mut system = peripherals.SYSTEM.split();
    let clocks = CLOCKS.init(ClockControl::boot_defaults(system.clock_control).freeze());

    let mut rtc = Rtc::new(peripherals.RTC_CNTL);
    let timer_group0 = TimerGroup::new(
        peripherals.TIMG0,
        &clocks,
        &mut system.peripheral_clock_control,
    );
    let mut wdt0 = timer_group0.wdt;
    let timer_group1 = TimerGroup::new(
        peripherals.TIMG1,
        &clocks,
        &mut system.peripheral_clock_control,
    );
    let mut wdt1 = timer_group1.wdt;

    // Disable watchdog timers
    rtc.swd.disable();
    rtc.rwdt.disable();
    wdt0.disable();
    wdt1.disable();

    embassy::init(&clocks, SystemTimer::new(peripherals.SYSTIMER));

    let _cpu_control = CpuControl::new(system.cpu_control);

    // Async requires the GPIO interrupt to wake futures
    hal::interrupt::enable(
        hal::peripherals::Interrupt::GPIO,
        hal::interrupt::Priority::Priority1,
    )
    .unwrap();

    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);
    let pulse = PulseControl::new(
        peripherals.RMT,
        &mut system.peripheral_clock_control,
        ClockSource::APB,
        0,
        0,
        0,
    )
    .unwrap();

    // Init I2C

    let sda = io.pins.gpio2;
    let scl = io.pins.gpio1;

    let i2c = I2C::new(
        peripherals.I2C0,
        sda,
        scl,
        100u32.kHz(),
        &mut system.peripheral_clock_control,
        &clocks,
    );

    // Create shared I2C Bus

    let i2c_busmanager = shared_bus::new_xtensa!(I2C<'static, I2C0> = i2c).unwrap();

    // Init SPI + DMA

    let sck = io.pins.gpio41;
    let mosi = io.pins.gpio42;
    let spi = Spi::new_no_cs_no_miso(
        peripherals.SPI2,
        sck,
        mosi,
        80u32.MHz(),
        hal::spi::SpiMode::Mode0,
        &mut system.peripheral_clock_control,
        &clocks,
    );

    let dma = Gdma::new(peripherals.DMA, &mut system.peripheral_clock_control);
    let dma_channel = dma.channel0;

    // Init Display backlight control

    let ledc = LEDC::new(
        peripherals.LEDC,
        clocks,
        &mut system.peripheral_clock_control,
    );

    // Init display early to clear pixel mash from screen

    let mut display = Display::new(
        spi,
        dma_channel,
        ledc,
        io.pins.gpio46,
        io.pins.gpio38,
        io.pins.gpio40,
    )
    .await;
    display.clear().await.unwrap();

    // Init uart

    let mut uart0 = Uart::new(peripherals.UART0, &mut system.peripheral_clock_control);
    uart0.set_rx_fifo_full_threshold(READ_BUF_SIZE as u16);

    hal::interrupt::enable(
        hal::peripherals::Interrupt::UART0,
        hal::interrupt::Priority::Priority2,
    )
    .unwrap();

    let _rng = RNG.init(Rng::new(peripherals.RNG));

    // Init Flow3r components

    let badgelink = BadgeLink::new(i2c_busmanager.acquire_i2c());
    let imu = ImuHandler::new(i2c_busmanager.acquire_i2c());
    let inputs = InputHandler;
    let captouch = CaptouchHandler;
    let leds = init_leds(pulse.channel0, io.pins.gpio14);

    let flow3r = Flow3r::new(badgelink, captouch, display, imu, inputs, leds);

    // Spawn background tasks

    let spawner = Spawner::for_current_executor().await;
    spawner
        .spawn(input_controller(
            i2c_busmanager.acquire_i2c(),
            io.pins.gpio8,
            io.pins.gpio0,
            io.pins.gpio3,
        ))
        .ok();
    spawner
        .spawn(captouch_controller(
            i2c_busmanager.acquire_i2c(),
            i2c_busmanager.acquire_i2c(),
            io.pins.gpio16,
            io.pins.gpio15,
        ))
        .ok();

    // Hand over to main task
    spawner.spawn(main(flow3r)).ok();
}
